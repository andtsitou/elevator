package by.epam.javaadvanced.elevatorb.view;

public class ConstantsView {

	public static final int defaultWindowWidth = 450;
	public static final int defaultWindowHeight = 500;
		
	public static final double SPEED_ANIMATION_BASIC = 1000;
	
	public static final double CORRECTION_SPEED_VALUE = 0.0001;
	
}