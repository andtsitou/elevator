package by.epam.javaadvanced.elevatorb.view.draw;

import java.awt.Graphics;
import java.util.Iterator;

import by.epam.javaadvanced.elevatorb.logic.bean.ConditionOfPassenger;
import by.epam.javaadvanced.elevatorb.logic.controllers.ElevatorController;
import by.epam.javaadvanced.elevatorb.logic.controllers.PassengerController;

public class GraphicSamples {

	private int passengerXDimension;
	private int passengerYDimension;
	
	private int counter;
	
	private int coordinateXPassengerStart;
	private int coordinateXPassengerEnd;
	
	private int pX;
	private int pY;
	
	private int tempCoordinateXPassenger;
	private int tempCoordinateYPassenger;
		
	public void drawPassenger(Graphics g, int floorHeight, int amountOfFloors, int allHeight, int allWidth, int elevatorXDimension, PassengerController passengerController){	
		passengerXDimension = 20;
		passengerYDimension = floorHeight - (int)(floorHeight*0.3);
		if(passengerController.getPassenger().getCondition() == ConditionOfPassenger.START | 
				passengerController.getPassenger().getCondition() == ConditionOfPassenger.AWAITING){
			coordinateXPassengerStart = (int) (0.02*allWidth);
			coordinateXPassengerEnd = (int) (0.5*allWidth - elevatorXDimension);
			pX = coordinateXPassengerStart +(int)(coordinateXPassengerEnd - passengerController.getPassenger().getRemainingDistance()*coordinateXPassengerEnd);
			pY = floorHeight * (amountOfFloors - passengerController.getPassenger().getStartFloor()) + (floorHeight - passengerYDimension);
		} else {
			if (passengerController.getPassenger().getCondition() == ConditionOfPassenger.LEAVING ){
				coordinateXPassengerStart = (int) (0.5*allWidth + elevatorXDimension);
				coordinateXPassengerEnd = (int) (allWidth*0.98);
				pX = allWidth - (int)((allWidth - coordinateXPassengerStart)*passengerController.getPassenger().getRemainingDistance());
				pY = floorHeight * (amountOfFloors - passengerController.getPassenger().getEndFloor()) + (floorHeight - passengerYDimension);
			}
			// else don't draw passenger (draw it in out of range of window)		
			else { 
				pX = allWidth + 50;
				pY = allHeight + 50;
			}
		}
		if (ConditionOfPassenger.FINISH != passengerController.getPassenger().getCondition()){
			drawPassengerByCoordinate(g, floorHeight, pX, pY, passengerController);
		}		
	}
	
	public void drawElevator(Graphics g, int floorHeight, int allHeight, int allWidth, ElevatorController elevatorController){
		int elevatorXDimension = (int) (allWidth*0.2);
		if (elevatorXDimension < 3*passengerXDimension){
			elevatorXDimension = 3*passengerXDimension;
		}
		
		int elevatorYDimension = (int) (allHeight/elevatorController.getElevator().getAmountOfFloors()*0.8) ;
		// draw elevator		
		g.drawRect(elevatorController.getCoordinateX(), elevatorController.getCoordinateY(), elevatorXDimension, elevatorYDimension);
	
		// draw passengers in lift. Use listOfLiftingPassengers in elevatorController
		Iterator<PassengerController> it = elevatorController.getListOfLiftingPassengers().iterator();
		counter = 0;
		while(it.hasNext()){
			PassengerController passengerController = it.next();
			if ( passengerXDimension*( 1 + elevatorController.getListOfLiftingPassengers().size()) < (elevatorXDimension - passengerXDimension)){
				tempCoordinateXPassenger = elevatorController.getCoordinateX() + (int)((counter+1)*passengerXDimension);
			} else{
				tempCoordinateXPassenger = elevatorController.getCoordinateX() + passengerXDimension + (int)((counter)*(elevatorXDimension - 2*passengerXDimension)/elevatorController.getListOfLiftingPassengers().size());
				
			}
			tempCoordinateYPassenger = elevatorController.getCoordinateY() + (elevatorYDimension - passengerYDimension);
			
			drawPassengerByCoordinate(g, 
				floorHeight,
				tempCoordinateXPassenger,
				tempCoordinateYPassenger,
				passengerController);			
			counter++;			
		}		
	}
	
	private void drawPassengerByCoordinate(Graphics g, int floorHeight, int pX, int pY, PassengerController passengerController){
		// draw passenger
		g.drawRect(pX, pY, passengerXDimension, passengerYDimension );
		// draw number of end floor to passenger
		g.drawString(Integer.toString(passengerController.getPassenger().getEndFloor()), 
			pX + (int)(passengerXDimension*0.1), 
			pY + (int)(passengerYDimension*0.8) );
	}	
}