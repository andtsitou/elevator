package by.epam.javaadvanced.elevatorb.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javaadvanced.elevatorb.logic.JTextAreaAppender;
import by.epam.javaadvanced.elevatorb.logic.ThreadsInspector;
import by.epam.javaadvanced.elevatorb.view.draw.PanelDrawer;

public class WindowView implements ChangeListener{
	private JTextField textFieldFloors;
	private JTextField textFieldPassengers;
	private JTextField textFieldElevatorCapacity;
	private JTextArea statusOutputText;
	private JPanel paintPanel;
	private JSlider slider;
	
	private ThreadsInspector threadsInspector;
	
	private Logger logger = LogManager.getFormatterLogger(ThreadsInspector.class);
		
	public void openWindow(){
		//Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
	}
	
	public void setThreadInspector(ThreadsInspector threadsInspector){
		this.threadsInspector = threadsInspector;
	}
	
	private void createAndShowGUI(){				
		 //Create and set up the window.
        JFrame frame = new JFrame("Elevator (by Andrei Tsitou)");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Create JPanel with 3 rows and 1 colon
        JPanel panel = new JPanel();
        
        GridBagLayout gridBagLayout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        panel.setLayout(gridBagLayout);
        
        c.fill = GridBagConstraints.NORTHEAST;
        c.weightx = 1;
        c.weighty = 0.2;
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        
        panel.setPreferredSize(new Dimension(ConstantsView.defaultWindowWidth, ConstantsView.defaultWindowHeight));
                
        //first row - buttons and text filds
        JPanel box = new JPanel();
        JLabel textLabelFloors = new JLabel("Floors: "); 
      
        textFieldFloors = new JTextField("2", 10 );
        textFieldFloors.setToolTipText("Enter number of floors.");     
        textLabelFloors.setLabelFor(textFieldFloors);
               
        JLabel textLabelPassengers = new JLabel("Passengers: ");
        
        textFieldPassengers = new JTextField("1", 10);
        textFieldPassengers.setToolTipText("Enter number of passengers.");
        textLabelPassengers.setLabelFor(textFieldPassengers);
        
        JLabel textLabelElevatorCapacity = new JLabel("Elevator capacity: ");
        
        textFieldElevatorCapacity = new JTextField("1", 10);
        textFieldElevatorCapacity.setToolTipText("Enter capacity of elevator.");
        textLabelElevatorCapacity.setLabelFor(textFieldElevatorCapacity);
       
        slider = new JSlider();
        slider.setMinimum(0);
        slider.setMaximum(100);
        slider.setValue(10);
        slider.setMajorTickSpacing(50);
        slider.setMinorTickSpacing(1);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setSnapToTicks(true);
        slider.addChangeListener(this);
                
        // button start
        JButton buttonStart = new JButton("Start");
        
        buttonStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				// create threads of elevator and passengers and start them all 
				int amountOfFloors = 0;
				int amountOfPassengers = 0;
				int elevatorCapacity = 0;
				try {
					amountOfFloors = Integer.parseInt(textFieldFloors.getText());
					amountOfPassengers = Integer.parseInt(textFieldPassengers.getText());
					elevatorCapacity = Integer.parseInt(textFieldElevatorCapacity.getText());
					if (amountOfFloors < 2  | 
							amountOfPassengers < 1 |
							elevatorCapacity < 1 
							) {
						throw new NumberFormatException("Entered value is illegal.");
					}				
				} catch (NumberFormatException e2) {
					logger.info("Illegal values was entered as start parameters. \n Standart  start parameters will be used.");
					amountOfFloors = 2;
					amountOfPassengers = 1;
					elevatorCapacity = 1;
				}
				logger.info("ENTERED PARAMETERS: floors=" + amountOfFloors + " passengers=" + amountOfPassengers + " elevator capacity=" + elevatorCapacity);
				threadsInspector.initStartParameters(elevatorCapacity, amountOfFloors, amountOfPassengers);
				threadsInspector.setSpeedAnimationInAnimationController(slider.getValue());
				threadsInspector.startExecution();
				paintPanel.repaint();
			}
		});        
        
        // button stop        
        JButton buttonStop = new JButton("Stop");
        buttonStop.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				// stop all threads. 
				threadsInspector.stopExecution();
			}
		});
       
        box.add(textLabelFloors);
        box.add(textFieldFloors);
        box.add(textLabelPassengers);
        box.add(textFieldPassengers);
        box.add(textLabelElevatorCapacity);
        box.add(textFieldElevatorCapacity);
        box.add(slider);
        box.add(buttonStart);
        box.add(buttonStop);
        box.setBorder(new EtchedBorder());
        
        // need to set nonresisable dimensions. maybe need change box to another container 
        panel.add(box,c);
        
        //Second row - graphical panel with elevator
        paintPanel = new PanelDrawer(threadsInspector);
        threadsInspector.setPanelDrawer((PanelDrawer)paintPanel);
        paintPanel.setBackground(Color.WHITE);
        paintPanel.setBorder(new EtchedBorder());
        paintPanel.repaint();
    
        c.fill = GridBagConstraints.CENTER;
        c.weightx = 1;
        c.weighty = 0.6;
        c.gridx = 0;
        c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;        
        panel.add(paintPanel, c);
        
//        Log        
        //Third row - text console.             
        statusOutputText = new JTextArea();
        JTextAreaAppender.addTextArea(statusOutputText); 
        JScrollPane scrol = new JScrollPane(statusOutputText);
        scrol.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrol.setBorder(new TitledBorder("Status output"));
        c.fill = GridBagConstraints.SOUTH;
        c.weightx = 1;
        c.weighty = 0.2;
        c.gridx = 0;
        c.gridy = 2;
        c.fill = GridBagConstraints.BOTH;       
        panel.add(scrol, c);
                
        //Add panel to the frame
        frame.add(panel);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
	}

	public void stateChanged(ChangeEvent e) {
		if (threadsInspector.getAnimationController() != null){
			threadsInspector.setSpeedAnimationInAnimationController(slider.getValue());
		}
	}	
}