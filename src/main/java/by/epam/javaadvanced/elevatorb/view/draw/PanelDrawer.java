package by.epam.javaadvanced.elevatorb.view.draw;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Iterator;

import javax.swing.JPanel;

import by.epam.javaadvanced.elevatorb.logic.ThreadsInspector;
import by.epam.javaadvanced.elevatorb.logic.bean.Direction;
import by.epam.javaadvanced.elevatorb.logic.controllers.ElevatorController;
import by.epam.javaadvanced.elevatorb.logic.controllers.PassengerController;

public class PanelDrawer extends JPanel {

	private static final long serialVersionUID = 10L;
	
	private ThreadsInspector threadsInspector = null;
	private ElevatorController elevatorController = null;
	private GraphicSamples graphicSamples = new GraphicSamples();
	
	private int amountOfFloors = 1;
	private int floorHeight = 1;
	
	private double elevatorXDimension;
	private double elevatorYDimension;
	private double elevatorXCoordinate;
	private double elevatorYCoordinate;
	
	public PanelDrawer(ThreadsInspector threadsInspector) {
		super();
		this.threadsInspector = threadsInspector;
	}
	
	public void repaintPanelDrawer() {
		super.repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		amountOfFloors = threadsInspector.getAmountOfFloors();
		elevatorController = threadsInspector.getElevatorController();
		g.setColor(Color.BLACK);
		floorHeight = (int) this.getHeight()/amountOfFloors;

		//draw floors
		for (int i=0; i < amountOfFloors; i++){			
			g.drawLine((int)(this.getWidth()*0.03), i*floorHeight, (int)(this.getWidth()*0.39), i*floorHeight);
			g.drawLine((int)(this.getWidth()*0.61), i*floorHeight, (int)(this.getWidth()*0.99), i*floorHeight);
			g.drawString(Integer.toString(amountOfFloors - i), 2, (int)((i+1)*floorHeight));
		}
		
		//draw passengers
		if (threadsInspector.getGlobalListOfPassengerControllers() != null ){
			Iterator<PassengerController> it = threadsInspector.getGlobalListOfPassengerControllers().iterator();
			while (it.hasNext()){
				PassengerController passengerController = it.next();
				graphicSamples.drawPassenger(g, floorHeight, amountOfFloors, this.getHeight(), this.getWidth(), (int) elevatorXDimension, passengerController);
			}
		}
		// draw elevator with passengers inside
		if (threadsInspector.getElevatorController() != null){
			elevatorXDimension = this.getWidth()*0.2;
			elevatorYDimension = floorHeight*0.8;
			
			elevatorXCoordinate = this.getWidth() * 0.5 - 0.5 * elevatorXDimension ;
			if (!elevatorController.isEnableNextStep()){							
				if ( Direction.UPSTAIR == elevatorController.getElevator().getDirection()) {
					elevatorYCoordinate = this.getHeight() + (floorHeight - elevatorYDimension) - (floorHeight * threadsInspector.getElevatorController().getElevator().getCurrentFloor()) - (floorHeight*(1 - threadsInspector.getElevatorController().getCoordinateYRelative()));
				} else {
					if (Direction.DOWNSTAIR == elevatorController.getElevator().getDirection()){
						elevatorYCoordinate = this.getHeight() + (floorHeight - elevatorYDimension) - (floorHeight * (threadsInspector.getElevatorController().getElevator().getCurrentFloor())) + (floorHeight*(1 - threadsInspector.getElevatorController().getCoordinateYRelative()));
					} 
					//else {}// do nothing when elevator.getDirection() = STOP or WAIT
				}				
			}
			// cast double coordinates to int
			elevatorController.setCoordinateX( (int) elevatorXCoordinate );
			elevatorController.setCoordinateY( (int) elevatorYCoordinate );			
			graphicSamples.drawElevator(g, floorHeight, this.getHeight(), this.getWidth(), threadsInspector.getElevatorController());
		}
	}
}
