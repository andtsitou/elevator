package by.epam.javaadvanced.elevatorb.view;

import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javaadvanced.elevatorb.logic.ThreadsInspector;
import by.epam.javaadvanced.elevatorb.logic.bean.ConditionOfPassenger;
import by.epam.javaadvanced.elevatorb.logic.bean.Direction;
import by.epam.javaadvanced.elevatorb.logic.bean.Elevator;
import by.epam.javaadvanced.elevatorb.logic.controllers.ElevatorController;
import by.epam.javaadvanced.elevatorb.logic.controllers.PassengerController;

public class SpeedController implements Runnable{
	private Logger logger = LogManager.getFormatterLogger(ThreadsInspector.class);
	ThreadsInspector threadsInspector = null;
	Elevator elevator = null;
	ElevatorController elevatorController = null;
	private int speedAnimation;	
	
	public SpeedController(ThreadsInspector threadsInspector) {
		super();
		this.threadsInspector = threadsInspector;
	}	
	public void run() {		
		elevatorController = threadsInspector.getElevatorController();
		elevator = elevatorController.getElevator();
		while (true) {			
			if (!threadsInspector.getElevatorController().isEnableNextStep()){
				if ( Direction.UPSTAIR == elevator.getDirection() | 
						Direction.DOWNSTAIR == elevator.getDirection()	) {
					threadsInspector.getElevatorController().makeAnimationRelativeStep(speedAnimation);
				}
			}			
			// animate passengers
			Iterator<PassengerController> it = threadsInspector.getGlobalListOfPassengerControllers().iterator();
			while (it.hasNext()){
				PassengerController passengerController = it.next();
				//make step for passenger
				synchronized (passengerController) {
					if(passengerController.getPassenger().getCondition() == ConditionOfPassenger.START | 
							passengerController.getPassenger().getCondition() == ConditionOfPassenger.LEAVING  ){
						passengerController.passengerMakeStep(speedAnimation);
						passengerController.notify();
					}
					else {
						passengerController.notify();
					}
				}
			}
			if( threadsInspector.getElevatorController().getCoordinateYRelative() < 0){				
				threadsInspector.getElevatorController().setCoordinateYRelative( 1 );     // start-value 1 set
				synchronized (threadsInspector.getElevatorController()) {
					threadsInspector.getElevatorController().setEnableNextStep(true);
					threadsInspector.getElevatorController().notify();
				}
			}			
			threadsInspector.getPanelDrawer().repaintPanelDrawer();			
			try {
				Thread.sleep(40);
			} catch (InterruptedException e) {
				logger.info("Animation execution is interupted: " + e.getMessage());
				break;
			}
		}
	}
	public void setAnimationSpeed(int speedAnimation) {
		this.speedAnimation = speedAnimation; 		
	}	
}
