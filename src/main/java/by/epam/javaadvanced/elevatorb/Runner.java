package by.epam.javaadvanced.elevatorb;

import by.epam.javaadvanced.elevatorb.logic.ThreadsInspector;
import by.epam.javaadvanced.elevatorb.view.WindowView;

/**
 * by Andrei Tsitou . 2015.12.10
 */
public class Runner {
    public static void main( String[] args ){
        ThreadsInspector threadsInspector = new ThreadsInspector();        
        WindowView windowView = new WindowView();        
        windowView.setThreadInspector(threadsInspector);        
        windowView.openWindow();        
    }
}
