package by.epam.javaadvanced.elevatorb.logic.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javaadvanced.elevatorb.logic.ThreadsInspector;
import by.epam.javaadvanced.elevatorb.logic.bean.ConditionOfPassenger;
import by.epam.javaadvanced.elevatorb.logic.bean.Passenger;

public class PassengerController implements Runnable {
	private Passenger passenger = null;
	private ElevatorController elevatorController = null;
	
	private Logger logger = LogManager.getFormatterLogger(ThreadsInspector.class);
	
	public PassengerController(Passenger passenger, ElevatorController elevatorController) {
		super();
		this.passenger = passenger;
		this.elevatorController = elevatorController;
	}
		
	public void run() {
		logger.info("Start passsenger thread.");		
		elevatorController.passengerHaveStarted(this);
		do {	// add paseenger to list. 
		} while (!elevatorController.addPassengerToListOfPassengersUpComing(this));		
		synchronized (elevatorController) {
			elevatorController.notify();
		}		
		while (passenger.getCondition() != ConditionOfPassenger.FINISH){			
			switch (passenger.getCondition()) {
			case LEAVING:	
				if (0 >= passenger.getRemainingDistance()){
					logger.info("Passenger has gone away.");
					passenger.setCondition(ConditionOfPassenger.FINISH);
					elevatorController.getListOfPassengersOutComming().remove(this);
				}
				break;
			case LIFTING:	
				if (elevatorController.getElevator().getCurrentFloor() == passenger.getEndFloor()){
					if (elevatorController.leaveElevator(this)){
						logger.info("Passenger has left the elevator");
						passenger.setCondition(ConditionOfPassenger.LEAVING);
						passenger.setRemainingDistance( 1 );  									// value 1 to set
						elevatorController.getListOfPassengersOutComming().add(this);
					}
					else
						elevatorController.stayInElevator(this);
				}
				else {
					elevatorController.stayInElevator(this);
				}
				synchronized ( elevatorController) {
					elevatorController.notify();
				}
				break;
			case AWAITING:	
				if (elevatorController.getElevator().getCurrentFloor() == passenger.getStartFloor()){
					if (elevatorController.enterInElevator(this)){
						logger.info("Passenger has entered into the elevator");
						passenger.setCondition(ConditionOfPassenger.LIFTING);
						elevatorController.getListOfPassengersUpComing().remove(passenger);
					} else {						
						elevatorController.stayInAwaitingListElevator(this);						
					}	
				} else {
					elevatorController.stayInAwaitingListElevator(this);					
				}
				synchronized (elevatorController) {
					elevatorController.notify();
				}
				break;			
			case START:
				if (0 >= passenger.getRemainingDistance()){
					logger.info("Passenger is waiting the elevator");
					passenger.setCondition(ConditionOfPassenger.AWAITING);
					elevatorController.addPassengerToListOfPassengersToAdd(this);	
				}				
				break;	
			default:
				break;
			}
			waitElevatorThread();	
		}		
		logger.info("Passenger thread is finished.");		
	}
	
	private synchronized void waitElevatorThread(){
		try {
			synchronized (this) {
				this.wait();
			}
		} catch (InterruptedException e) {
			logger.info("Passenger thread is interupted: " + e.getMessage());
			passenger.setCondition(ConditionOfPassenger.FINISH);
		}
	}	
	
	public synchronized void passengerMakeStep(int speedAnimation){
		passenger.makeStep(speedAnimation);
	}		
	public Passenger getPassenger(){
		return passenger;
	}
}