package by.epam.javaadvanced.elevatorb.logic.bean;

import by.epam.javaadvanced.elevatorb.logic.LogicConstants;

public class Passenger {	
	private int startFloor;
	private int endFloor;
	private double speed;
	private double remainingDistance = LogicConstants.DISTANCE;
	
	private ConditionOfPassenger condition = ConditionOfPassenger.START;

	public Passenger(int startFloor, int endFloor, double speed) {
		super();
		this.startFloor = startFloor;
		this.endFloor = endFloor;
		this.speed = speed;
	}

	public ConditionOfPassenger getCondition() {
		return condition;
	}
	public void setCondition(ConditionOfPassenger condition) {
		this.condition = condition;
	}
	public int getStartFloor() {
		return startFloor;
	}
	public int getEndFloor() {
		return endFloor;
	}
	public double getRemainingDistance() {
		return remainingDistance;
	}
	public void setRemainingDistance(int remainingDistance) {
		this.remainingDistance = remainingDistance;
	}
	public double getSpeed() {
		return speed;
	}	
	public void makeStep(int speedAnimation){
		if (remainingDistance < speed){
			remainingDistance = 0;
		}
		else {
			remainingDistance = remainingDistance - speed*speedAnimation;
		}		
	}
}