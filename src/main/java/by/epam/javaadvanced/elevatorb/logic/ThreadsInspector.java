package by.epam.javaadvanced.elevatorb.logic;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javaadvanced.elevatorb.logic.bean.Elevator;
import by.epam.javaadvanced.elevatorb.logic.bean.Passenger;
import by.epam.javaadvanced.elevatorb.logic.controllers.ElevatorController;
import by.epam.javaadvanced.elevatorb.logic.controllers.PassengerController;
import by.epam.javaadvanced.elevatorb.logic.factories.FactoryOfPassengers;
import by.epam.javaadvanced.elevatorb.view.SpeedController;
import by.epam.javaadvanced.elevatorb.view.draw.PanelDrawer;

public class ThreadsInspector {
	private Logger logger = LogManager.getFormatterLogger(ThreadsInspector.class);
	
	private ExecutorService executor = null;
	private Elevator elevator = null;
	private ElevatorController elevatorController = null;
	private SpeedController animationController = null;
	private Thread animationThread = null; 
	private FactoryOfPassengers factoryOfPassengers = null;
	private PanelDrawer panelDrawer = null;
	
	private int amountOfFloors = 1;
	
	@SuppressWarnings("unused")
	private volatile int speedAnimation = 1;
		
	private List<PassengerController> globalListOfPassengerControllers = null;
	
	public void initStartParameters(int elevatorCapacity, int amountOfFloors, int amountOfPassengers){
		executor = Executors.newCachedThreadPool();
		speedAnimation = 10;
		logger.info( " START EXECUTON OF PROGRAM " );
		elevator = new Elevator(elevatorCapacity, amountOfFloors);
		this.amountOfFloors = amountOfFloors;
		factoryOfPassengers = new FactoryOfPassengers(elevator);
		elevatorController = new ElevatorController();
		elevatorController.initStartParameters(elevator, amountOfPassengers);
		
		globalListOfPassengerControllers = new LinkedList<PassengerController>();
		
		for (int i=0 ; i<amountOfPassengers ; i++){
			Passenger passenger = factoryOfPassengers.getNewPassenger();			
			globalListOfPassengerControllers.add(new PassengerController(passenger, elevatorController));						
		}		
		animationController = new SpeedController(this);
		animationThread = new Thread(animationController);
		animationThread.start();
		
	}	
	
	public void startExecution(){		
		executor.submit(elevatorController);
		Iterator<PassengerController> it = globalListOfPassengerControllers.iterator();		
		while ( !elevatorController.getEnableStartPassenger() ){
			// wait while elevator-thread start
		}
		// start passengers threads		
		while (it.hasNext()){
			PassengerController passengerController = it.next();
			executor.submit(passengerController);	
		}		
	}
		
	public void stopExecution(){
		try {
			executor.shutdownNow();	
			animationThread.interrupt();
		} catch (SecurityException e) {
			logger.info("All threads is interupted.");
		} 
	}
	
	public int getAmountOfFloors(){
		return amountOfFloors;
	}
	public List<PassengerController> getGlobalListOfPassengerControllers() {
		return globalListOfPassengerControllers;
	}
	public ElevatorController getElevatorController() {
		return elevatorController;
	}
	public PanelDrawer getPanelDrawer() {
		return panelDrawer;
	}
	public void setPanelDrawer(PanelDrawer panelDrawer) {
		this.panelDrawer = panelDrawer;
	}
	public void setSpeedAnimationInAnimationController(int speedAnimation) {
		this.speedAnimation = speedAnimation;
		animationController.setAnimationSpeed(speedAnimation);
	}
	public SpeedController getAnimationController() {
		return animationController;
	}		
}