package by.epam.javaadvanced.elevatorb.logic;

public class LogicConstants {
	
	public static final double DISTANCE = 1;
		
	public static final int PASSENGER_MAX_SPEED = 20;
	
	public static final int PASSENGER_DISTANCE = 50;
	
	public static final int CORRECTION_VALUE = 1;
	
	public static final double CORRECTION_SPEED_VALUE = 0.01;

}
