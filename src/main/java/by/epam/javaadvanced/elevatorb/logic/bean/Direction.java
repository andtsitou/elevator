package by.epam.javaadvanced.elevatorb.logic.bean;

public enum Direction {
	UPSTAIR, DOWNSTAIR, WAIT, STOP;
}