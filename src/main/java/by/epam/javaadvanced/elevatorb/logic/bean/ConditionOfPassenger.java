package by.epam.javaadvanced.elevatorb.logic.bean;

public enum ConditionOfPassenger {
	START, AWAITING, ENTER_IN_LIFT, LIFTING, LEAVE_LIFT, LEAVING, FINISH 	
}
