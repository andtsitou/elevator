package by.epam.javaadvanced.elevatorb.logic.controllers;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javaadvanced.elevatorb.logic.LogicConstants;
import by.epam.javaadvanced.elevatorb.logic.ThreadsInspector;
import by.epam.javaadvanced.elevatorb.logic.bean.Direction;
import by.epam.javaadvanced.elevatorb.logic.bean.Elevator;

public class ElevatorController implements Runnable {
	
	private volatile Elevator elevator = null;
	private int amountOfAllPassengers = 0;
	
	private volatile List<PassengerController> listOfLiftingPassengers = null;
	private List<PassengerController> listOfAwatingPassengersByFloor[] = null;
	private List<PassengerController> listOfPassengersToAdd = null;
	private List<PassengerController> listOfPassengersUpComing = null;	
	private List<PassengerController> listOfPassengersOutComming = null;	
	private volatile AtomicInteger atomicCounter = new AtomicInteger(0); 
	private volatile AtomicInteger atomicCounterAwaiting = new AtomicInteger(0);	

	// enable execution of elevator thread when all passengers thread is started.
	private volatile boolean enableNextStep = false;
	
	private AtomicInteger atomicStartedPassengerCounter = new AtomicInteger(0);	
	private volatile boolean enableStartPassenger = false;	
	private volatile boolean elevatorExecutionIsEnd = false;
	
	// relative coordinate for motion animation
	private volatile double coordinateYRelative = 1;
	private double speedYRelative = 0.008;
	
	// absolute coordinate for draw function of animation
	private int coordinateX = 0;
	private int coordinateY = 0;
	
	private Logger logger = LogManager.getFormatterLogger(ThreadsInspector.class);

	@SuppressWarnings("unchecked")
	public void initStartParameters(Elevator elevator, int amountOfAllPassengers){
		this.elevator = elevator;
		this.amountOfAllPassengers = amountOfAllPassengers;
		listOfLiftingPassengers = Collections.synchronizedList(new LinkedList<PassengerController>());	
		listOfAwatingPassengersByFloor = new List[elevator.getAmountOfFloors()];
				for ( int i=0; i < elevator.getAmountOfFloors() ; i++){
			listOfAwatingPassengersByFloor[i] = Collections.synchronizedList(new LinkedList<PassengerController>());
		}
		listOfPassengersToAdd = Collections.synchronizedList(new LinkedList<PassengerController>());
		listOfPassengersUpComing = Collections.synchronizedList(new LinkedList<PassengerController>());				
		listOfPassengersOutComming = Collections.synchronizedList(new LinkedList<PassengerController>());		
	}

	public void run() {
		logger.info(" Start elevator thread.");		
		Thread.currentThread().setName("Elevator controller");	
		atomicStartedPassengerCounter.set(amountOfAllPassengers);
		enableStartPassenger = true;
		while ( atomicStartedPassengerCounter.get() > 0 ){
			try {
				synchronized (this) {
					this.wait();	
				}				
			} catch (InterruptedException e) {				
				logger.info("Elevator is enterupted. " + e.getMessage());
				elevatorExecutionIsEnd = true;
			}			
		}
		
		// main cycle of elevator-thread life
		while (!elevatorExecutionIsEnd){		
			logger.info("Elevator has arrived to " + elevator.getCurrentFloor()+ " floor.");			
			notifyLiftingPassengers();			
			addPassengerFromListToAddToListByFloor();			
			notifyAwaitingPassengers();			
			goToNextFloor();			
			checkElevatorExecutionIsEnd();			
		}
		logger.info("Elevator thread is finished.");
	}
		
	private synchronized void checkElevatorExecutionIsEnd() {
		if(listOfLiftingPassengers.isEmpty()){
			if(listOfPassengersToAdd.isEmpty()){
				if(listOfPassengersUpComing.isEmpty()){
					boolean isEmpty = false;
					for (int i = 0 ; i<elevator.getAmountOfFloors(); i++){
						if(listOfAwatingPassengersByFloor[i].isEmpty()){
							isEmpty = true;
						} else{
							isEmpty = false;
							break;
						}
					}
					elevatorExecutionIsEnd = isEmpty;
				}
			}
		}		
	}

	private synchronized void addPassengerFromListToAddToListByFloor() {
		List<PassengerController> currentList = listOfPassengersToAdd;
		if (!currentList.isEmpty()){
			Iterator<PassengerController> it = currentList.iterator();
			while(it.hasNext()){
				PassengerController passengerController = it.next();
				it.remove();				
				listOfAwatingPassengersByFloor[passengerController.getPassenger().getStartFloor() - LogicConstants.CORRECTION_VALUE].add(passengerController);
			}			
		}
		//  else {}  // do nothing. listOfPassengersToAdd is empty		
	}

	private synchronized void notifyLiftingPassengers() {	
		if (!listOfLiftingPassengers.isEmpty()){
			atomicCounter.set( 0 ); 				// initial set in 0;				
			Iterator<PassengerController> it = listOfLiftingPassengers.iterator();
			atomicCounter.set(listOfLiftingPassengers.size());
			do {
				PassengerController passengerController = it.next();

				synchronized (passengerController){
					passengerController.notify();
				}
			} while ( it.hasNext() );			
			while ( atomicCounter.get() > 0 ) {
				// wait passenger-thread, which were notified
				try {
					synchronized (this) {
						this.wait();
					}
				} catch (InterruptedException e) {
					logger.info("Elevator is enterupted. " + e.getMessage());
					elevatorExecutionIsEnd = true;
				}
			}			
		}
		//else {} // No lifting passenger to notify
	}
	
	public synchronized boolean leaveElevator(PassengerController passengerController) {
		if (elevator.decrementCurrentLoad()){
			boolean result = listOfLiftingPassengers.remove(passengerController);
			if (result) {
				atomicCounter.decrementAndGet();
				listOfPassengersOutComming.add(passengerController);
				return true;
			}	
		}		
		else {
			return false;
		}
		return false;		
	}	
	public synchronized boolean stayInElevator(PassengerController passengerController) {
		atomicCounter.decrementAndGet();
		return true;
	}
	
	private synchronized void notifyAwaitingPassengers() {
		if (elevator.getCapacity() > elevator.getCurrentLoad()){
			if ( !listOfAwatingPassengersByFloor[elevator.getCurrentFloor() - LogicConstants.CORRECTION_VALUE].isEmpty()){
				Iterator<PassengerController> it = listOfAwatingPassengersByFloor[elevator.getCurrentFloor() - LogicConstants.CORRECTION_VALUE].iterator();
				
				// notify only amount passengers equal to amount of free space in elevator
				int counter = 0;
				if ((elevator.getCapacity() - elevator.getCurrentLoad()) 
						< 
						listOfAwatingPassengersByFloor[elevator.getCurrentFloor() - LogicConstants.CORRECTION_VALUE].size()){
					counter = (elevator.getCapacity() - elevator.getCurrentLoad());
					atomicCounterAwaiting.set(counter);
				} else {
					counter = listOfAwatingPassengersByFloor[elevator.getCurrentFloor() - LogicConstants.CORRECTION_VALUE].size();
					atomicCounterAwaiting.set(counter);
				}
				for (int i = 0; i<counter ; i++) {
					PassengerController passengerController = it.next();
					synchronized (passengerController){
						passengerController.notify();
					}
				}			
				// elevator wait while atomicCurrentAmount become zero(0) 
				//or while capacity of elevator become max					
				while (atomicCounterAwaiting.get() > 0){
					if (elevator.getCurrentLoad() == elevator.getCapacity()){
						break;
					}
					try {
						synchronized (this) {
							this.wait();
						}						
					} catch (InterruptedException e) {
						logger.info("Elevator is enterupted. " + e.getMessage());
						elevatorExecutionIsEnd = true;
					}
				}				
			} 
			//else{}   //  Elevator has nobody to notify. Go to the next floor			
		} else {
			logger.info("Elevator is full. Go to the next floor.");
		}		
	}
	public synchronized boolean enterInElevator(PassengerController passengerController) {
		if (elevator.getCurrentLoad() < elevator.getCapacity() ){
			if ( !enableNextStep){
				return false;
			}					
			if (elevator.getCurrentFloor() == passengerController.getPassenger().getStartFloor()){
				// add passenger controller to list of lifting passengers
				if (elevator.incrementCurrentLoad()){
					listOfLiftingPassengers.add(passengerController);
				}
				else {
					return false;
				}
				// remove passenger controller from list by floor awating passengers		
				listOfAwatingPassengersByFloor[passengerController.getPassenger().getStartFloor()-LogicConstants.CORRECTION_VALUE].remove(passengerController);
				atomicCounterAwaiting.decrementAndGet();
				return true;
			}
			else {
				return false;
			}			
		}
		else { 		// Elefator is full.
			return false;
		}
	}
	public synchronized boolean stayInAwaitingListElevator(PassengerController passengerController) {
		atomicCounterAwaiting.decrementAndGet();
		return true;
	}

	private void goToNextFloor() {
		awaitAnimationToNextStep();
		elevator.nextFloor();
		if (elevator.getDirection() == Direction.UPSTAIR | 
				elevator.getDirection() == Direction.DOWNSTAIR	){
			coordinateYRelative = 1;
		}
		//else {}	// do nothing		
	}
	
	private void awaitAnimationToNextStep(){
		enableNextStep = false;
		while (!enableNextStep){
			try {
				synchronized (this) {
					this.wait();
				}
			} catch (InterruptedException e) {				
				logger.info("Elevator is enterupted. " + e.getMessage());
				elevatorExecutionIsEnd = true;
			}			
		}
	}
	
	
	public Elevator getElevator(){
		return elevator;
	}	
	public synchronized boolean addPassengerToListOfPassengersToAdd(PassengerController passengerController){
		listOfPassengersUpComing.remove(passengerController);			
		return listOfPassengersToAdd.add(passengerController);
	}	
	public synchronized boolean addPassengerToListOfPassengersUpComing(PassengerController passengerController){
		return listOfPassengersUpComing.add(passengerController);
	}
	public boolean getEnableStartPassenger(){
		return enableStartPassenger;
	}
	public synchronized void passengerHaveStarted(PassengerController passengerController){
		atomicStartedPassengerCounter.decrementAndGet();
	}
	public void makeAnimationRelativeStep(int speedAnimation){
		coordinateYRelative -= speedYRelative*speedAnimation;
	}	
	public List<PassengerController> getListOfLiftingPassengers() {
		return listOfLiftingPassengers;
	}	
	public boolean isEnableNextStep() {
		return enableNextStep;
	}
	public void setEnableNextStep(boolean enableArrivalElevator) {
		this.enableNextStep = enableArrivalElevator;
	}
	public int getCoordinateX() {
		return coordinateX;	}
	public void setCoordinateX(int coordinateX) {
		this.coordinateX = coordinateX;
	}
	public int getCoordinateY() {
		return coordinateY;
	}
	public void setCoordinateY(int coordinateY) {
		this.coordinateY = coordinateY;
	}
	public double getCoordinateYRelative() {
		return coordinateYRelative;
	}
	public void setCoordinateYRelative(double coordinateYRelative) {
		this.coordinateYRelative = coordinateYRelative;
	}		
	public double getSpeedYRelative() {
		return speedYRelative;
	}
	public void setSpeedYRelative(double speedYRelative) {
		this.speedYRelative = speedYRelative;
	}	
	public List<PassengerController> getListOfPassengersUpComing() {
		return listOfPassengersUpComing;
	}
	public List<PassengerController> getListOfPassengersOutComming() {
		return listOfPassengersOutComming;
	}			
}