package by.epam.javaadvanced.elevatorb.logic.factories;

import java.util.Random;

import by.epam.javaadvanced.elevatorb.logic.LogicConstants;
import by.epam.javaadvanced.elevatorb.logic.bean.Elevator;
import by.epam.javaadvanced.elevatorb.logic.bean.Passenger;

public class FactoryOfPassengers {
	private Random rand = new Random();	
	private int startFloor;
	private int endFloor;
	private double speed;
	
	private int amountOfFloors;
	public FactoryOfPassengers(Elevator elevator) {
		super();
		amountOfFloors = elevator.getAmountOfFloors();
	}
	
	public Passenger getNewPassenger(){		
		//correct all values to + 1 (to exclude zero vulue); 
		startFloor = LogicConstants.CORRECTION_VALUE + rand.nextInt(amountOfFloors);
		do {
			endFloor = LogicConstants.CORRECTION_VALUE + rand.nextInt(amountOfFloors);
		} while (startFloor == endFloor);
		speed = LogicConstants.CORRECTION_SPEED_VALUE + rand.nextDouble()/2000;
		
		return new Passenger(startFloor, endFloor, speed);		
	}	
}
