package by.epam.javaadvanced.elevatorb.logic.bean;

public class Elevator {

	private int capacity = 1;					// from 1 and higher
	private int amountOfFloors = 2;				// from 2 and higher
	
	private volatile Integer currentFloor = 1; 			// from 1 to max value
	private volatile Integer currentLoad = 0;
	private Direction direction = Direction.UPSTAIR;
	
	public Elevator(int capacity, int amountOfFloors) {
		super();
		this.capacity = capacity;
		this.amountOfFloors = amountOfFloors;
	}

	public void nextFloor(){
		if (direction == Direction.UPSTAIR){
			if (currentFloor < amountOfFloors){
				currentFloor++;
			}
			else {
				direction = Direction.DOWNSTAIR;
				currentFloor--;
			}
		}
		else{
			if (currentFloor > 1){
				currentFloor--;
			}
			else{
				direction = Direction.UPSTAIR;
				currentFloor++;
			}
		}
		if (currentFloor == 1){
			direction = Direction.UPSTAIR;			
		}		
		if (currentFloor == amountOfFloors) {
			direction = Direction.DOWNSTAIR;					
		}
	}

	public Direction getDirection() {
		return direction;
	}
	public int getAmountOfFloors() {		
		return amountOfFloors;
	}
	public int getCapacity() {
		return capacity;
	}
	public Integer getCurrentFloor() {
		return currentFloor;
	}
	public Integer getCurrentLoad() {
		return currentLoad;
	}
	public void setCurrentLoad(Integer currentLoad) {
		this.currentLoad = currentLoad;
	}
	public boolean incrementCurrentLoad(){
		if (currentLoad < capacity){
			currentLoad++;
			return true;
		}
		else {
			return false;
		}
	}
	public boolean decrementCurrentLoad(){
		if (currentLoad > 0){
			currentLoad--;
			return true;
		}
		else {
			return false;
		}
	}	
}
